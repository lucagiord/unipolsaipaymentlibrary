# UnipolSaiPayments

[![CI Status](https://img.shields.io/travis/lucagiordanosky/UnipolSaiPayments.svg?style=flat)](https://travis-ci.org/lucagiordanosky/UnipolSaiPayments)
[![Version](https://img.shields.io/cocoapods/v/UnipolSaiPayments.svg?style=flat)](https://cocoapods.org/pods/UnipolSaiPayments)
[![License](https://img.shields.io/cocoapods/l/UnipolSaiPayments.svg?style=flat)](https://cocoapods.org/pods/UnipolSaiPayments)
[![Platform](https://img.shields.io/cocoapods/p/UnipolSaiPayments.svg?style=flat)](https://cocoapods.org/pods/UnipolSaiPayments)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

UnipolSaiPayments is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'UnipolSaiPayments'
```

## Author

lu.giordano@reply.it

## License

UnipolSaiPayments is available under the MIT license. See the LICENSE file for more info.
