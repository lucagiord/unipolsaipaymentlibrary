//
//  Dictionary+Union.swift
//  UnipolSaiApp
//

import Foundation

extension Dictionary {
    func union(other:Dictionary) -> Dictionary {
        var result = self
        
        for (key,value) in other {
            result.updateValue(value, forKey:key)
        }
        
        return result
    }
}
