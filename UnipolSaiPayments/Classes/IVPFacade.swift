//Facade.swift

import Foundation
import Alamofire

private struct EndPoints {
    static let parkingsBaseURL = "parcheggi"
    static let stoppingBaseURL = "soste"
    static let userBaseURL = "utenti"
    static let statusBaseURL = "stati"
    static let ordersBaseURL = "ordini"
    static let debtsBaseURL = "debiti"
    static let paytipperPaymentBaseURL = "pagamenti/avviaPagamento"
    static let paytipperCallbackBaseURL = "pagamenti/notificaPagamento"
    static let initilizePaymentsTokenBaseURL = "metodiPagamento/token"
    static let paymentMethodsBaseURL = "metodiPagamento"
    static let createUserBillingSystemBaseURL = "pagamenti/abilitaUtente"
    static let reservationsBaseURL = "prenotazioni"
}

public class IVPFacade {
    
    public static var shared = IVPFacade()
    private static var dateformatter = DateFormatter()
    var baseURL : String = ""
    
    public typealias ServiceCompletionBlock = ((_ response: Any?, _ error: Error?) -> ())
    public typealias NoResponseServiceCompletionBlock = ((_ error: Error?) -> ())
    
    
//     *** parcheggi ***

    public func getParkings(url: String? = nil, completion: @escaping ServiceCompletionBlock) {
        print("baseURL \(baseURL)")
        
        let endPoint : String = "\(baseURL)\(url ?? EndPoints.parkingsBaseURL)"
        print("endPoint \(endPoint)")
        IVPNetworkService.shared.getService(endPoint: endPoint, headers: [:]) { (json, error) in
            if let json = json as? [String : Any] {
                if let data = IVPDataParser.shared.parseMyParkings(json: json) {
                    completion(data, nil)
                } else {
                    //completion(nil, ErrorManager.getInstance.errorGeneric(error: json))
                    print("Errore")
                }
            } else if let error = error {
                completion(nil, error)
            }
        }
    }
    
    public func getParkingById(parkingId: String, completion: @escaping ServiceCompletionBlock) {
        let endPoint = "\(EndPoints.parkingsBaseURL)/\(parkingId)"

        IVPNetworkService.shared.getService(endPoint: endPoint, headers: [:]) { (json, error) in
            if let json = json as? [String : Any] {
                if let data = IVPDataParser.shared.parseMyParking(json: json) {
                    completion(data, nil)
                } else {
                    //completion(nil, ErrorManager.getInstance.errorGeneric(error: json))
                    print("Errore")
                }
            } else if let error = error {
                completion(nil, error)
            }
        }
    }

    public func postParkingReservation(parkingId: String, invoice: [String : Any], completion: @escaping ServiceCompletionBlock) {

        let endPoint = "\(EndPoints.parkingsBaseURL)/\(parkingId)/prenotazione"

        IVPNetworkService.shared.postService(endPoint: endPoint, headers: [:], body: invoice) { (json, error) in
            if let json = json as? [String : Any] {
                if let data = IVPDataParser.shared.parseMyParkingReservation(json: json) {
                    completion(data, nil)
                } else {
                    //completion(nil, ErrorManager.getInstance.errorGeneric(error: json))
                    print("Errore")
                }
            } else if let error = error {
                completion(nil, error)
            }
        }
    }

    public func postCancelParkingReservation(parkingId: String, reservationId: String, invoice: [String : Any], completion: @escaping ServiceCompletionBlock) {

        let endPoint = "\(EndPoints.parkingsBaseURL)/\(parkingId)/prenotazione/\(reservationId)"

        IVPNetworkService.shared.postService(endPoint: endPoint, headers: [:], body: invoice) { (json, error) in
            if let json = json as? [String : Any] {
                if let data = IVPDataParser.shared.parseMyParkingInvoice(json: json) {
                    completion(data, nil)
                } else {
                    //completion(nil, ErrorManager.getInstance.errorGeneric(error: json))
                    print("Errore")
                }
            } else if let error = error {
                completion(nil, error)
            }
        }
    }

    public func postParkingAccess(parkingId: String, licensePlate: String, parkingAndInvoice: [String : Any], completion: @escaping ServiceCompletionBlock) {

        let endPoint = "\(EndPoints.parkingsBaseURL)/\(parkingId)/accesso/\(licensePlate)"

        IVPNetworkService.shared.postService(endPoint: endPoint, headers: [:], body: parkingAndInvoice) { (json, error) in
            if let json = json as? [String : Any] {
                if let data = IVPDataParser.shared.parseParkingAccess(json: json) {
                    completion(data, nil)
                } else {
                    //completion(nil, ErrorManager.getInstance.errorGeneric(error: json))
                    print("Errore")
                }
            } else if let error = error {
                completion(nil, error)
            }
        }
    }

    public func postParkingCostsEstimation(parkingId: String, licensePlate: String, parking: [String : Any], completion: @escaping ServiceCompletionBlock) {

        let endPoint = "\(EndPoints.parkingsBaseURL)/\(parkingId)/pagamento/\(licensePlate)/stima"

        IVPNetworkService.shared.postService(endPoint: endPoint, headers: [:], body: parking) { (json, error) in
            if let json = json as? [String : Any] {
                if let data = IVPDataParser.shared.parseParkingCostsEstimation(json: json) {
                    completion(data, nil)
                } else {
                    //completion(nil, ErrorManager.getInstance.errorGeneric(error: json))
                    print("Errore")
                }
            } else if let error = error {
                completion(nil, error)
            }
        }
    }
    
    public func getStoppings(completion: @escaping ServiceCompletionBlock) {

        IVPNetworkService.shared.getService(endPoint: EndPoints.stoppingBaseURL, headers: [:]) { (json, error) in
            if let json = json as? [String : Any] {
                if let data = IVPDataParser.shared.parseStoppings(json: json) {
                    completion(data, nil)
                } else {
                    //completion(nil, ErrorManager.getInstance.errorGeneric(error: json))
                    print("Errore")
                }
            } else if let error = error {
                completion(nil, error)
            }
        }
    }

    public func postUserPay(stoppingId: String, licensePlate: String, parkingAndInvoice: [String : Any], completion: @escaping ServiceCompletionBlock) {

        let endPoint = "\(EndPoints.stoppingBaseURL)/\(stoppingId)/pagamento/\(licensePlate)"

        IVPNetworkService.shared.postService(endPoint: endPoint, headers: [:], body: parkingAndInvoice) { (json, error) in
            if let json = json as? [String : Any] {
                if let data = IVPDataParser.shared.parseUserPay(json: json) {
                    completion(data, nil)
                } else {
                    //completion(nil, ErrorManager.getInstance.errorGeneric(error: json))
                    print("Errore")
                }
            } else if let error = error {
                completion(nil, error)
            }
        }
    }
    
    public func postUserExit(stoppingId: String, invoice: [String : Any], completion: @escaping ServiceCompletionBlock) {
        
        let endPoint = "\(EndPoints.stoppingBaseURL)/\(stoppingId)/uscita"
        
        IVPNetworkService.shared.postService(endPoint: endPoint, headers: [:], body: invoice) { (json, error) in
            if let json = json as? [String : Any] {
                if let data = IVPDataParser.shared.parseUserExit(json: json) {
                    completion(data, nil)
                } else {
                    //completion(nil, ErrorManager.getInstance.errorGeneric(error: json))
                    print("Errore")
                }
            } else if let error = error {
                completion(nil, error)
            }
        }
    }

    public func userProfileCreation(userData: [String : Any], completion: @escaping ServiceCompletionBlock) {

        IVPNetworkService.shared.postService(endPoint: EndPoints.userBaseURL, headers: [:], body: userData) { (json, error) in
            if let json = json as? [String : Any] {
                if let data = IVPDataParser.shared.parseUserProfileData(json: json) {
                    completion(data, nil)
                } else {
                    //completion(nil, ErrorManager.getInstance.errorGeneric(error: json))
                    print("Errore")
                }
            } else if let error = error {
                completion(nil, error)
            }
        }
    }

    public func getUserProfile(completion: @escaping ServiceCompletionBlock) {

        IVPNetworkService.shared.getService(endPoint: EndPoints.userBaseURL, headers: [:]) { (json, error) in
            if let json = json as? [String : Any] {
                if let data = IVPDataParser.shared.parseUserProfileData(json: json) {
                    completion(data, nil)
                } else {
                    //completion(nil, ErrorManager.getInstance.errorGeneric(error: json))
                    print("Errore")
                }
            } else if let error = error {
                completion(nil, error)
            }
        }
    }

    public func getParkingStatus(completion: @escaping ServiceCompletionBlock) {

        IVPNetworkService.shared.getService(endPoint: EndPoints.statusBaseURL, headers: [:]) { (json, error) in
            if let json = json as? [String : Any] {
                if let data = IVPDataParser.shared.parseParkingStatus(json: json) {
                    completion(data, nil)
                } else {
                    //completion(nil, ErrorManager.getInstance.errorGeneric(error: json))
                    print("Errore")
                }
            } else if let error = error {
                completion(nil, error)
            }
        }
    }
    
    public func getUserDebts(completion: @escaping ServiceCompletionBlock) {
        
        IVPNetworkService.shared.getService(endPoint: EndPoints.debtsBaseURL, headers: [:]) { (json, error) in
            if let json = json as? [String : Any] {
                if let data = IVPDataParser.shared.parseUserDebts(json: json) {
                    completion(data, nil)
                } else {
                    //completion(nil, ErrorManager.getInstance.errorGeneric(error: json))
                    print("Errore")
                }
            } else if let error = error {
                completion(nil, error)
            }
        }
    }
    
    public func getUserSingleDebt(debtId: String, completion: @escaping ServiceCompletionBlock) {
        
        let endPoint = "\(EndPoints.debtsBaseURL)/\(debtId)"
        
        IVPNetworkService.shared.getService(endPoint: endPoint, headers: [:]) { (json, error) in
            if let json = json as? [String : Any] {
                if let data = IVPDataParser.shared.parseUserSingleDebt(json: json) {
                    completion(data, nil)
                } else {
                    //completion(nil, ErrorManager.getInstance.errorGeneric(error: json))
                    print("Errore")
                }
            } else if let error = error {
                completion(nil, error)
            }
        }
    }
    
    public func changeUserDebt(debtId: String, status: [String : Any], completion: @escaping ServiceCompletionBlock) {
        
        let endPoint = "\(EndPoints.debtsBaseURL)/\(debtId)"
        
        IVPNetworkService.shared.postService(endPoint: endPoint, headers: [:], body: status) { (json, error) in
            if let json = json as? [String : Any] {
                if let data = IVPDataParser.shared.parseUserSingleDebt(json: json) {
                    completion(data, nil)
                } else {
                    //completion(nil, ErrorManager.getInstance.errorGeneric(error: json))
                    print("Errore")
                }
            } else if let error = error {
                completion(nil, error)
            }
        }
    }
    
    public func getReservations(completion: @escaping ServiceCompletionBlock) {
        
        IVPNetworkService.shared.getService(endPoint: EndPoints.reservationsBaseURL, headers: [:]) { (json, error) in
            if let json = json as? [String : Any] {
                if let data = IVPDataParser.shared.parseReservations(json: json) {
                    completion(data, nil)
                } else {
                    //completion(nil, ErrorManager.getInstance.errorGeneric(error: json))
                    print("Errore")
                }
            } else if let error = error {
                completion(nil, error)
            }
        }
    }


    // *** pagamenti ***


    public func getUserPayments(completion: @escaping ServiceCompletionBlock) {

        IVPNetworkService.shared.getService(endPoint: EndPoints.ordersBaseURL, headers: [:]) { (json, error) in
            if let json = json as? [String : Any] {
                if let data = IVPDataParser.shared.parseUserPayments(json: json) {
                    completion(data, nil)
                } else {
                    //completion(nil, ErrorManager.getInstance.errorGeneric(error: json))
                    print("Errore")
                }
            } else if let error = error {
                completion(nil, error)
            }
        }
    }
    
    public func createUserInBillingSystem(userId: [String : Any], completion: @escaping NoResponseServiceCompletionBlock) {
        
        IVPNetworkService.shared.postService(endPoint: EndPoints.createUserBillingSystemBaseURL, headers: [:], body: userId) { (json, error) in
            if let json = json as? [String : Any] {
                IVPDataParser.shared.parseEmptyResponse(json: json) ? print("Errore") : completion(nil)
            } else if let error = error {
                completion(error)
            }
        }
    }

    public func paytipperPaymentRequest(requestData: [String : Any], completion: @escaping NoResponseServiceCompletionBlock) {

        IVPNetworkService.shared.postService(endPoint: EndPoints.paytipperPaymentBaseURL, headers: [:], body: requestData) { (json, error) in
            if let json = json as? [String : Any] {
                IVPDataParser.shared.parseEmptyResponse(json: json) ? print("Errore") : completion(nil)
            } else if let error = error {
                completion(error)
            }
        }
    }

    public func paytipperCallback(completion: @escaping NoResponseServiceCompletionBlock) {

        IVPNetworkService.shared.postService(endPoint: EndPoints.paytipperCallbackBaseURL, headers: [:], body: [:]) { (json, error) in
            if let json = json as? [String : Any] {
                IVPDataParser.shared.parseEmptyResponse(json: json) ? print("Errore") : completion(nil)
            } else if let error = error {
                completion(error)
            }
        }
    }

    public func initializePaymentTokens(requestData: [String : Any], completion: @escaping ServiceCompletionBlock) {

        IVPNetworkService.shared.postService(endPoint: EndPoints.initilizePaymentsTokenBaseURL, headers: [:], body: requestData) { (json, error) in
            if let json = json as? [String : Any] {
                if let data = IVPDataParser.shared.parsePaymentToken(json: json) {
                    completion(data, nil)
                } else {
                    //completion(nil, ErrorManager.getInstance.errorGeneric(error: json))
                    print("Errore")
                }
            } else if let error = error {
                completion(nil, error)
            }
        }
    }

    public func managePaymentMethod(paymentMethodId: String, completion: @escaping NoResponseServiceCompletionBlock) {

        let endPoint = "\(EndPoints.paymentMethodsBaseURL)/\(paymentMethodId)"

        IVPNetworkService.shared.postService(endPoint: endPoint, headers: [:], body: [:]) { (json, error) in
            if let json = json as? [String : Any] {
                IVPDataParser.shared.parseEmptyResponse(json: json) ? print("Errore") : completion(nil)
            } else if let error = error {
                completion(error)
            }
        }
    }
    
    public func getPaymentMethods(completion: @escaping ServiceCompletionBlock) {

        IVPNetworkService.shared.getService(endPoint: EndPoints.paymentMethodsBaseURL, headers: [:]) { (json, error) in
            if let json = json as? [String : Any] {
                if let data = IVPDataParser.shared.parseUserPaymentMethods(json: json) {
                    completion(data, nil)
                } else {
                    //completion(nil, ErrorManager.getInstance.errorGeneric(error: json))
                    print("Errore")
                }
            } else if let error = error {
                completion(nil, error)
            }
        }
    }
    
    // *** print ***
    
    public func generateAccountBalancePdf(orderId: String, data: [String : Any], completion: @escaping ServiceCompletionBlock) {
        
        let endPoint = "\(EndPoints.ordersBaseURL)/\(orderId)/estrattoConto"
        
        IVPNetworkService.shared.postService(endPoint: endPoint, headers: [:], body: data) { (json, error) in
            if let json = json as? [String : Any] {
                if let data = IVPDataParser.shared.parsePdfOrXslxGenerationRequest(json: json) {
                    completion(data, nil)
                } else {
                    //completion(nil, ErrorManager.getInstance.errorGeneric(error: json))
                    print("Errore")
                }
            } else if let error = error {
                completion(nil, error)
            }
        }
    }

}

extension IVPFacade {
    
    public func stringToTimeStamp(string: String, format: String = "dd/MM/yyyy") -> Double? {

        IVPFacade.dateformatter.dateFormat = format
        let date = IVPFacade.dateformatter.date(from: string)
        if let date = date {
            return date.timeIntervalSince1970
        }
        return nil
    }
}
