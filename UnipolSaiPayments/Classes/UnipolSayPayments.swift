//UnipolSayPayments.swift

import Foundation

public class UnipolSayPayments {
    public static func setBaseURL(baseURL : String) {
        IVPFacade.shared.baseURL = baseURL
    }
}
