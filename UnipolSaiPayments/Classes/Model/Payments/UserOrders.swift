//
//  UserOrders.swift
//  UnipolSaiPayments

//payload di risposta della chiamata GET /ordini

import Foundation

class UserOrders: NSObject {
    var payments: [InvoiceResponse]?
}
