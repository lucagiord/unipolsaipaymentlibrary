//
//  StartPaymentResponse.swift
//  UnipolSaiPayments

//payload di riposta alla chiamata POST /pagamenti/avviaPagamento
//payload di riposta alla chiamata POST /pagamenti/notificaPagamento

import Foundation

class StartPaymentResponse: NSObject {
    var state: Int?
    var message: String?
    var detail: String?
}
