//
//  AccountBalanceRequest.swift
//  UnipolSaiPayments

//payload della chiamata a POST /ordini/{idOrdine}/estrattoConto

import Foundation

class AccountBalanceRequest: NSObject {
    var licensePlate: String?
    var startParkingDate: String?
    var endParkingDate: String?
    var service: String?
    var totalAmount: String?
    var paymentId: String?
    var parkingName: String?
}
