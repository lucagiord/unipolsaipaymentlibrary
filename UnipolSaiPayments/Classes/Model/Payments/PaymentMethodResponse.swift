//
//  PaymentMethodResponse.swift
//  UnipolSaiPayments

//payload di risposta alla chiamata POST /metodiPagamento/token

import Foundation

enum responseCodeEnum: String {
    case correctlyConcluded = "000"
    case invalidKeyapp = "100"
    case invalidPaymentChannel = "101"
    case falseMetadataFormat = "102"
    case paymentAlreadyComplete = "103"
    case notexistingToken = "104"
    case invalidToken = "105"
    case notPresentTransaction = "106"
    case alreadyAssociatedToken = "107"
    case paymentIdNotFound = "108"
    case unauthorizedTransaction = "109"
    case genericError = "999"
}

class PaymentMethodResponse: NSObject {
    var tokenId: String?
    var tokenAlias: String?
    var responseCode: responseCodeEnum?
    var responseDescription: String?
}
