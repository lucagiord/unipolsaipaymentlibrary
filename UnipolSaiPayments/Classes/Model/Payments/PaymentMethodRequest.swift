//
//  PaymentMethodRequest.swift
//  UnipolSaiPayments

//payload della chiamata POST /metodiPagamento/token

import Foundation

class PaymentMethodRequest: NSObject {
    var requestId: String?
    var preferredPaymentMethod: Bool = true
}
