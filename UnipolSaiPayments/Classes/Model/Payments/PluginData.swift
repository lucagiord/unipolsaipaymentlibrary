//
//  PluginData.swift
//  UnipolSaiPayments

enum paymentBrand: String {
    case VISA = "VISA"
    case MasterCard = "MasterCard"
    case Amex = "Amex"
    case Diners = "Diners"
    case Jcb = "Jcb"
    case Maestro = "Maestro"
    case MYBANK = "MYBANK"
    case SCT = "SCT"
    case SDD = "SDD"
    case CC = "CC"
    case Masterpass = "Masterpass"
    case SOFORT = "SOFORT"
    case PAYPAL = "PAYPAL"
    case AMAZONPAY = "AMAZONPAY"
}

enum channelType: String {
    case xpay = "XPAY"
}

import Foundation

class PluginData: NSObject {
    var isPreferred: Bool = false
    var isPreauthorized: Bool = true
    var token: String?
    var aliasToken: String?
    var pan: String?
    var brand: paymentBrand?
    var expirationDate: String?
    var channel: channelType?
}
