//
//  AccountBalanceResponse.swift
//  UnipolSaiPayments

//payload della risposta alla chiamata a POST /ordini/{idOrdine}/estrattoConto

import Foundation

enum documentType: Int {
    case pdf = 1
    case xslx = 2
}

class AccountBalanceResponse: NSObject {
    var document: String?
    var type: documentType?
}
