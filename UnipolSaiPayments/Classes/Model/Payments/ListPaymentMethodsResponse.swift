//
//  ListPaymentMethodsResponse.swift
//  UnipolSaiPayments

//payload di riposta alla chiamata GET /metodiPagamento

import Foundation

class ListPaymentMethodsResponse: NSObject {
    var listPaymentMethods: [Plugin]?
}
