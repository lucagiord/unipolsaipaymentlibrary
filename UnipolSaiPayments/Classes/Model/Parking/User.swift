//
//  User.swift
//  UnipolSaiPayments

//payload della chiamata POST /utenti

import Foundation

class User: NSObject {
    var name: String?
    var surname: String?
    var phoneNumber: String?
    var email: String?
    var idUserQBox: Int?
    var licensePlates: [String]?
    var userCompanyId: String?
}
