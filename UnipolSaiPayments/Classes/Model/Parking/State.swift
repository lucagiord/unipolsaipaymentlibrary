//
//  States.swift
//  UnipolSaiPayments


import Foundation

class State: NSObject {
    var parkings: [StoppingState]?
    var amount: Int?
    var delFreeReservationRemain: Int?
    var debts: Int?
}
