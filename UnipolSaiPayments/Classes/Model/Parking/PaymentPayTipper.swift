//
//  PaymentPayTipper.swift
//  Alamofire


import Foundation

enum payTipperStatus: String {
    case ok = "OK"
    case ko = "KO"
}

class PaymentPayTipper: NSObject {
    var paymentStatus: payTipperStatus?
    var paymentId: Int?
    var paymentDate: String?
    var controlKey: String?
}
