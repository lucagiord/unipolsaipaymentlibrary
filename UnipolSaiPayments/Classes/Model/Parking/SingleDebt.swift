//
//  SingleDebt.swift
//  UnipolSaiPayments

//payload di risposta della chiamata al servizio GET /debiti/{idDebito}

import Foundation

class SingleDebt: NSObject {
    var debt: UserDebt?
}
