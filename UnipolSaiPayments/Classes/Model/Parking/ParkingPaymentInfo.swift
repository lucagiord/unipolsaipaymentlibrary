//
//  ParkingPaymentInfo.swift
//  UnipolSaiPayments


import Foundation

class ParkingPaymentInfo: NSObject {
    var durationInHour: String?
    var paymentDate: String?
    var paymentMethods: PaymentMethod?
}
