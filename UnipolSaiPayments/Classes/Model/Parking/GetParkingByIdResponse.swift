//
//  GetParkingByIdResponse.swift
//  UnipolSaiPayments

//payload di risposta al servizio GET /parcheggi/{idParcheggio}

import Foundation

class GetParkingByIdResponse: NSObject {
    var parking: Parking?
}
