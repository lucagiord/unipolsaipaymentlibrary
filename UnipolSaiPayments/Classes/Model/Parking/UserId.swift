//
//  UserId.swift
//  UnipolSaiPayments

//payload della chiamata al servizio POST /pagamenti/abilitaUtente

import Foundation

class UserId: NSObject {
    var userId: String?
}
