//
//  Payment.swift
//  UnipolSaiPayments

import Foundation

enum transactionResultEnum: String {
    case ok = "OK"
    case error = "ERROR"
}

enum paymentMethodEnum: String {
    case credit_card = "CREDIT_CARD"
}

class Payment: NSObject {
    var paymentId: String?
    var transationResult: transactionResultEnum?
    var payedAmount: String?
    var remainedAmount: String?
    var paymentMethods: paymentMethodEnum?
}
