//
//  ReservationParkingDetails.swift
//  UnipolSaiPayments


import Foundation

class ReservationParkingDetails: NSObject {
    var totalAmount: Int?
    var startDate: String?
    var endDate: String?
    var summary: [ReservationSummary]?
}
