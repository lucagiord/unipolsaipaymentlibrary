//
//  Debt.swift
//  UnipolSaiPayments

import Foundation

enum debtStatusEnum: String {
    case pagato = "PAGATO"
    case in_attesa = "IN_ATTESA"
    case annullato = "ANNULLATO"
}

enum debtTypeEnum: String {
    case prenotazioni_consentite_superate = "PRENOTAZIONI_CONSENTITE_SUPERATE"
}

class Debt: NSObject {
    var debtId: String?
    var debtStatus: debtStatusEnum?
    var amount: Int?
    var desc: String?
    var debtType: debtTypeEnum?
    var relatedEntity: [String]?
    var creationDate: String?
    var modificationDate: String?
}
