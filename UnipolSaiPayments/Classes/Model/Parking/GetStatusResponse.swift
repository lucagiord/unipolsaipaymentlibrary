//
//  GetStatusResponse.swift
//  UnipolSaiPayments

//payload di risposta della chiamata GET /stati

import Foundation

class GetStatusResponse: NSObject {
    var state: State?
}
