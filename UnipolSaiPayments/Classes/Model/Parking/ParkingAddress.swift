//
//  ParkingAddress.swift
//  UnipolSaiPayments


import Foundation

class ParkingAddress: NSObject {
    var address: String?
    var city: String?
    var cap: String?
    var province: String?
    var nation: String?
}
