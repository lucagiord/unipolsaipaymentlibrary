//
//  ParkingPayment.swift
//  UnipolSaiPayments

//payload della chiamata POST /soste/{idSosta}/pagamento/{targa}

import Foundation

class ParkingPayment: NSObject {
    var parking: ParkingPaymentInfo?
    var invoice: Invoice?
}
