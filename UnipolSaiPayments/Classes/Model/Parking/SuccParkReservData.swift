//
//  SuccParkReservData.swift
//  UnipolSaiPayments


import Foundation

//payload della risposta alla chiamata POST /parcheggi/{idParcheggio}/prenotazione

class SuccParkReservData: NSObject {
    var parking: ParkingShort?
    var invoice: InvoiceResponse?
}
