//
//  ParkingCostsResponse.swift
//  UnipolSaiPayments

//payload di risposta della chiamata POST /parcheggi/{idParcheggio}/accesso/{targa}
//payload di risposta della chiamata POST /soste/{idSosta}/uscita

import Foundation

class ParkingCostsResponse: NSObject {
    var parking: ParkingStop?
    var invoice: InvoiceResponse?
}
