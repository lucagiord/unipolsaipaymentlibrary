//
//  ParkingRequest.swift
//  UnipolSaiPayments


import Foundation

class ParkingRequest: NSObject {
    var requestId: String?
    var requestType: String?
    var creationDate: String?
    var amount: Int?
    var idPaymentMethod: String?
    var receiptInformations: [ReceiptInfo]?
}
