//
//  EstimatedCost.swift
//  UnipolSaiPayments

import Foundation

class EstimatedCost: NSObject {
    var estimationId: String?
    var parkingId: String?
    var startDate: String?
    var durationInHour: String?
    var licensePlate: String?
    var totalAmount: String?
}
