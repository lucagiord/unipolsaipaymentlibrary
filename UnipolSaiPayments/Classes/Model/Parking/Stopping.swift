//
//  Stopping.swift
//  UnipolSaiPayments

import Foundation

class Stopping: NSObject {
    var stoppingId: String?
    var parkingId: String?
    var parkingName: String?
    var exitPin: String?
    var stoppingStartDate: String?
    var stoppingEndDate: String?
    var licensePlate: String?
    var parkingStatus: stoppingStateEnum?
    var amount: String?
    var payments: [Payment]?
}
