//
//  ParkingCostsRequest.swift
//  UnipolSaiPayments

//payload della chiamata POST /parcheggi/{idParcheggio}/accesso/{targa}

import Foundation

class ParkingCostsRequest: NSObject {
    var parking: ParkingShortInfo?
    var invoice: Invoice?
}
