//
//  ParkingAccess.swift
//  UnipolSaiPayments

//payload della chiamata POST /parcheggi/{idParcheggio}/pagamento/{targa}/calcola
//payload della risposta alla chiamata POST /parcheggi/{idParcheggio}/pagamento/{targa}/calcola

import Foundation

class ParkingAccessRequest: NSObject {
    var parking: ParkingEstimatedCost?
}
