//
//  ExitToken.swift
//  UnipolSaiPayments


import Foundation

class ExitToken: NSObject {
    var channel: String?
    var token: String?
}
