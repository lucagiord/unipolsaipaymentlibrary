//
//  GetParkingsResponse.swift
//  UnipolSaiPayments

//payload di risposta al servizio GET /parcheggi

import Foundation

public class GetParkingsResponse: NSObject {
    var parkings: [Parking]?
}
