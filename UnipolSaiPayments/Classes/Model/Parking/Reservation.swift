//
//  Reservation.swift
//  UnipolSaiPayments

import Foundation

enum reservationParkingStatusEnum: String {
    case active = "ACTIVE"
}

class Reservation: NSObject {
    var reservationId: String?
    var parkingStatus: reservationParkingStatusEnum?
    var startingDate: String?
    var endingDate: String?
    var parkingId: String?
    var licensePlate: String?
}

