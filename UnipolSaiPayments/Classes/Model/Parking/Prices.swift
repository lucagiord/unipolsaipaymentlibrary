//
//  Prices.swift
//  UnipolSaiPayments


import Foundation

enum carCategory: String {
    case a = "A"
    case b = "B"
    case c = "C"
    case d = "D"
    case e = "E"
    case f = "F"
}

class Prices: NSObject {
    var segment: carCategory?
    var openingTimes: [OpeningTime]?
    var maxDailyPrice: Int?
}
