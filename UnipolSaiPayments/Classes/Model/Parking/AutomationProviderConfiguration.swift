//
//  AutomationProviderConfiguration.swift
//  UnipolSaiPayments


import Foundation

class AutomationProviderConfiguration: NSObject {
    var hasPedestrianEntrance: Bool?
    var automationProviderCode: Int = 0
    var integrationProtocolCode: Int = 0
}
