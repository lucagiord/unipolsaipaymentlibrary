//
//  OpeningTime.swift
//  UnipolSaiPayments


import Foundation

enum openingDays: String {
    case lunedi = "LUNEDI"
    case martedi = "MARTEDI"
    case mercoledi = "MERCOLEDI"
    case giovedi = "GIOVEDI"
    case venerdi = "VENERDI"
    case sabato = "SABATO"
    case domenica = "DOMENICA"
    case festivi = "FESTIVI"
}

class OpeningTime: NSObject {
    var day: openingDays?
    var timeIntervals: [TimeInterval]?
    var isOpen24Hours: Bool?
    var isClose: Bool?
}
