//
//  NextOpeningInfo.swift
//  UnipolSaiPayments


import Foundation

class NextOpeningInfo: NSObject {
    var nextOpening: String?
    var nextOpeningDetail: String?
}
