//
//  ReceiptInfo.swift
//  UnipolSaiPayments

enum requestStatus: String {
    case ok = "OK"
    case deleted = "DELETED"
}

import Foundation

class ReceiptInfo: NSObject {
    var date: String?
    var descr: String?
    var amount: Int?
    var requestStatus: requestStatus?
    var serviceData: ServiceDataInvoice?
    var payments: [Payments]?
}
