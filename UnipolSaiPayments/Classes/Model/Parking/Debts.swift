//
//  Debts.swift
//  UnipolSaiPayments

//payload di risposta della chiamata al servizio GET /debiti

import Foundation

class Debts: NSObject {
    var debts: [Debt]?
}
