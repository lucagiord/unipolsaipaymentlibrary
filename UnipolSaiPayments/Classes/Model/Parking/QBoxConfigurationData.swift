//
//  QBoxConfigurationData.swift
//  UnipolSaiPayments


import Foundation

class QBoxConfigurationData: NSObject {
    var qBoxId: Int?
    var info: String?
}
