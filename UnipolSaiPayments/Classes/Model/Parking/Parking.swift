//
//  Parking.swift
//  UnipolSaiPayments

import Foundation

enum accessControlType: String {
    case automatico = "AUTOMATICO"
    case manuale = "MANUALE"
}

enum parkingState: String {
    case prossima_apertura = "PROSSIMA_APERTURA"
    case disponibile = "DISPONIBILE"
    case chiuso = "CHIUSO"
    case completo = "COMPLETO"
}

enum parkingAdditionalServices: String {
    case lavaggi_auto = "LAVAGGIO_AUTO"
    case ricarica_elettrica = "RICARICA_ELETTRICA"
    case custodia_chiavi = "CUSTODIA_CHIAVI"
    case parcheggio_custodito = "PARCHEGGIO_CUSTODITO"
    case gpl_gas_consentito = "GPL_GAS_CONSENTITO"
}

class Parking: NSObject {
    var parkingId: String?
    var businessName: String?
    var parkingName: String?
    var parkingAddress: ParkingAddress?
    var latitude: Double?
    var longitude: Double?
    var timeZone: String?
    var parkingPhone: String?
    var maxHeightAllowed: Int?
    var accessControlType: accessControlType?
    var state: parkingState?
    var qBoxConfiguration: [QBoxConfigurationData]?
    var secondsToClose: Int?
    var nextOpeningInfo: NextOpeningInfo?
    var automationProviderConfiguration: AutomationProviderConfiguration?
    var additionalServices: [parkingAdditionalServices]?
    var prices: Prices?
}
