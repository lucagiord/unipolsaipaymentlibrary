//
//  ParkingPaymentResponse.swift
//  UnipolSaiPayments

//payload della risposta alla chiamata POST /soste/{idSosta}/pagamento/{targa}

import Foundation

class ParkingPaymentResponse: NSObject {
    var parking: ParkingEnteringInfo?
    var invoice: InvoiceResponse?
}
