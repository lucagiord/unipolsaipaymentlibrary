//
//  Stoppings.swift
//  UnipolSaiPayments

// payload di ritorno della chiamata al servizio GET /soste

import Foundation

class Stoppings: NSObject {
    var stoppings: [Stopping]?
}
