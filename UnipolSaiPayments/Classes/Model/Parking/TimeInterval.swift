//
//  TimeInterval.swift
//  UnipolSaiPayments


import Foundation

class TimeInterval: NSObject {
    var startingTime: String?
    var endingTime: String?
    var parkingPrices: [ParkingPrice]?
}
