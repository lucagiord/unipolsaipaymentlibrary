//
//  Invoice.swift
//  UnipolSaiPayments


//payload della chiamata a POST /pagamenti/avviaPagamento

import Foundation

class Invoice: NSObject {
    var requestId: String?
    var requestData: InStructureParkingData?
    var paymentData: Payments?
}
