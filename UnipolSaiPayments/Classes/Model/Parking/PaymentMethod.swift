//
//  PaymentMethod.swift
//  UnipolSaiPayments


import Foundation

class PaymentMethod: NSObject {
    var paymentMethodId: String?
    var paymentMethodType: String?
}
