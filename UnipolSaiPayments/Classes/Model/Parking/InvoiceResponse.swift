//
//  InvoiceResponse.swift
//  UnipolSaiPayments

import Foundation

class InvoiceResponse: NSObject {
    var reservationId: String?
    var reservationType: String?
    var partnerId: String?
    var userId: String?
    var idBillableAccount: String?
    var creationDate: String?
    var reservationData: ReservationData?
    var requests: [ParkingRequest]?
    var detailData: ReservationParkingDetails?
}
