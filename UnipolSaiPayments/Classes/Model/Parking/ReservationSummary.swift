//
//  ReservationSummary.swift
//  UnipolSaiPayments

import Foundation

class ReservationSummary: NSObject {
    var amount: Int?
    var date: String?
    var descr: String?
}
