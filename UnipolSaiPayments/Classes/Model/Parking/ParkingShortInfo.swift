//
//  ParkingShortInfo.swift
//  UnipolSaiPayments


import Foundation

class ParkingShortInfo: NSObject {
    var reservationId: String?
    var tokenAccessQBox: String?
    var userIdQBox: String?
}
