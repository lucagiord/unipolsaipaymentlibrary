//
//  ParkingEnteringInfo.swift
//  UnipolSaiPayments


import Foundation

enum parkingEnteringState: String {
    case in_progress = "IN_PROGRESS"
    case paid = "PAID"
    case finished = "FINISHED"
}

class ParkingEnteringInfo: NSObject {
    var startDate: String?
    var endDate: String?
    var parkingDuration: String?
    var licensePlate: String?
    var parkingId: String?
    var reservationId: String?
    var parkingState: parkingEnteringState?
    var exitPin: String?
}
