//
//  CancelReservationParkingReq.swift
//  UnipolSaiPayments

//payload della risposta alla chiamata POST /parcheggi/{idParcheggio}/prenotazione/{idPrenotazione}

import Foundation

class CancelReservationParkingReq: NSObject {
    var invoice: InvoiceResponse?
}
