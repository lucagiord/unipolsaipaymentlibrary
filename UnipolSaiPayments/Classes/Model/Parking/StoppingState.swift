//
//  StoppingState.swift
//  UnipolSaiPayments


import Foundation

enum stoppingStateEnum: String {
    case prenotato = "PRENOTATO"
    case in_corso = "IN_CORSO"
    case pagato = "PAGATO"
    case finito = "FINITO"
}

class StoppingState: NSObject {
    var stoppingState: stoppingStateEnum?
    var licensePlate: String?
    var parkingId: String?
    var amount: Int?
    var nextPaymentCost: Int?
    var secondsToNextPayment: Int?
    var stoppingId: String?
    var reservationId: String?
    var payments: [String]?
    var exitTokens: [ExitToken]?
}
