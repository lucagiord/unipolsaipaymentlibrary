//
//  ParkingShort.swift
//  UnipolSaiPayments

import Foundation

class ParkingShort: NSObject {
    var reservationId: String?
    var reservationStatus: String?
    var reservationDate: String?
    var reservationEndDate: String?
    var parkingId: String?
    var licensePlate: String?
    var idStop: String?
}
