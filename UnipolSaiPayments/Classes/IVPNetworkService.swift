//
//  USNetworkService.swift
//  UnipolSaiApps

import Foundation
import Alamofire
//import IBMMobileFirstPlatformFoundation

enum environment: String {
    case svil
    case coll
    case mobile
}


class IVPNetworkService : NSObject {
    @objc public static let shared = IVPNetworkService()
    
    //TODO: Check se non c'è MFP trovare un altro modo per recuperare la base url
    @objc public static var baseUrl: URL! {
        if let plist = Bundle.main.path(forResource: "mfpclient", ofType: "plist") {
            if let plistDict = NSDictionary.init(contentsOfFile: plist) {
                return URL(string:"\(plistDict["protocol"] ?? "https")://\(plistDict["host"] ?? "")/")
            }
        }
        
//        return nil
        return  URL(string: "http://www.mocky.io")!
    }
    
    static let kCounter = 2
    var counter: Int = kCounter
    
    static let loginUrl = URL(string: "")!//  baseUrl.appendingPathComponent("")
    static let logoutUrl = URL(string: "")!// baseUrl.appendingPathComponent("")
    static let apicTokenUrl = URL(string: "")// URL(string: "\(IVPNetworkService.baseUrl!)")
    static let pushTokenUrl = URL(string: "")//URL(string: "\(IVPNetworkService.baseUrl!)")
    static let ibmClientStringsUrl = URL(string: "")!//baseUrl.appendingPathComponent("")
    
    static let env: String = "" //baseUrl.host?.components(separatedBy: ".")[0] ?? ""
    
    var headers: [String : String]?
    
    var authorization: [String : String]?
    
    static let configHeaders: [String : String] = ["authorization" : "",
                                                   "cache-control" : "no-cache",
                                                   "content-type" : "application/x-www-form-urlencoded",
                                                   "source" : "mobile"]
    
    static let loginHeaders: [String : String] = ["content-type" : "application/x-www-form-urlencoded",
                                                  "source" : "mobile"]
    
    
    private static var manager: Alamofire.SessionManager = {
        let configuration = URLSessionConfiguration.default
        
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        configuration.timeoutIntervalForRequest = 60
        
        var serverTrustPolicies: [String: ServerTrustPolicy]? = nil
//        serverTrustPolicies = [
//            "" : .disableEvaluation,
//        ]
        
        if let trustPolicy = serverTrustPolicies {
            return Alamofire.SessionManager(configuration: configuration, serverTrustPolicyManager: ServerTrustPolicyManager(policies: trustPolicy))
            
        } else {
            return Alamofire.SessionManager(configuration: configuration)
        }
        
    }()
    
    public func getService(endPoint: String, completion: @escaping (_ responseDict: Any?, _ error: Error?) -> Void) {
        self.getService(endPoint: endPoint, headers: [:], completion: completion)
    }
    
    public func getService(endPoint: String, headers: Dictionary<String, String>, completion: @escaping (_ responseDict: Any?, _ error: Error?) -> Void) {
        
        let endUrl = endPoint.replacingOccurrences(of: " ", with: "")
        //let initialURL : String = urlBase ?? IVPNetworkService.baseUrl!
        //var request = URLRequest(url: URL(string: "\(IVPNetworkService.baseUrl!)\(endUrl)")!)
        var request = URLRequest(url: URL(string: endUrl)!)
        
        print("request \(String(describing: request.url))")
        request.httpMethod = HTTPMethod.get.rawValue
        request.allHTTPHeaderFields = self.headers?.union(other: headers)
        
        callService(request: request, completion: completion)
    }
    
    public func postService(endPoint: String, body: Dictionary<String, Any>, completion: @escaping (_ responseDict: Any?, _ error: Error?) -> Void) {
        self.postService(endPoint: endPoint, headers: [:], body: body, completion: completion)
    }
    
    public func delecteCookie () {
        let cookieJar = HTTPCookieStorage.shared
        
        for cookie in cookieJar.cookies! {
            cookieJar.deleteCookie(cookie)
        }
    }
    
    public func postService(endPoint: String, headers: Dictionary<String, String>, body: Dictionary<String, Any>, completion: @escaping (_ responseDict: Any?, _ error: Error?) -> Void) {
        let endUrl = endPoint.replacingOccurrences(of: " ", with: "")
        //var request = URLRequest(url: URL(string: "\(IVPNetworkService.baseUrl!)\(endUrl)")!)
        var request = URLRequest(url: URL(string: endUrl)!)

        let jsonData = try? JSONSerialization.data(withJSONObject: body)
        
        request.httpMethod = HTTPMethod.post.rawValue
        request.allHTTPHeaderFields = self.headers?.union(other: headers)
        request.httpBody = jsonData
        
        callService(request: request, completion: completion)
    }
    
    public func postService(endPoint: String, data: String, completion: @escaping (_ responseDict: Any?, _ error: Error?) -> Void) {
        self.postService(endPoint: endPoint, headers: [:], data: data, completion: completion)
    }
    
    public func postService(endPoint: String, headers: Dictionary<String, String>, data: String, completion: @escaping (_ responseDict: Any?, _ error: Error?) -> Void) {
        let endUrl = endPoint.replacingOccurrences(of: " ", with: "")
        //var request = URLRequest(url: URL(string: "\(IVPNetworkService.baseUrl!)\(endUrl)")!)
        var request = URLRequest(url: URL(string: endUrl)!)

        request.httpMethod = HTTPMethod.post.rawValue
        request.allHTTPHeaderFields = self.headers?.union(other: headers)
        request.httpBody = data.data(using: .utf8)
        
        callService(request: request, completion: completion)
    }
    
    //TOCO: Check se non c'è MFP si può rimuovere
    public func deleteService(endPoint: String, headers: Dictionary<String, String>, body: Dictionary<String, Any>, completion: @escaping ( _ responseDict: Any?,  _ error: Error?) -> Void) {
        
        var request = URLRequest(url: IVPNetworkService.pushTokenUrl!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.allHTTPHeaderFields = IVPNetworkService.configHeaders
        request.httpBody = "".data(using: .utf8)
        
        callService(request: request) { (responseDict, error) in
            
            guard let token = responseDict as? [String:Any],  let token_type = token["token_type"], let access_token = token["access_token"] else {
                #if DEBUG
                    print("SERVICE: no token_type or access_token")
                #endif
                completion(nil, error)
                return
            }
            
            var newRequest = URLRequest(url: URL(string: "\(IVPNetworkService.baseUrl!)\(endPoint)")!)
            
            newRequest.httpMethod = HTTPMethod.delete.rawValue
            let authString = "\(token_type) \(access_token)"
            newRequest.allHTTPHeaderFields = ["Authorization" : authString]
            
            let when = DispatchTime.now() + 10 // change 2 to desired number of seconds
            DispatchQueue.main.asyncAfter(deadline: when) {
                // Your code with delay
                self.callService(request: newRequest, completion: completion)
            }
        }
    }
    
    @discardableResult
    func callService(request:URLRequest, completion: @escaping (_ responseDict: Any?, _ error: Error?) -> Void) -> DataRequest? {
        let request = IVPNetworkService.manager.request(request).responseJSON { response in
            if response.response?.statusCode ?? 0 < 200 || response.response?.statusCode ?? 0 > 299 {
                
                if response.response?.statusCode == 401 {
                    self.reloadConfig(response: response, completion: completion)
                    return
                }
                
                #if DEBUG
                    print("Error \(request.url?.absoluteString ?? ""): \(String(describing: response.result.error))")
                #endif
                
                let error = NSError(domain: "", code: response.response?.statusCode ?? 0, userInfo: nil)
                completion(response.result.value as? [String : Any], error)
                return
            }
            
            guard let responseJSON = response.result.value else {
                #if DEBUG
                    print("Response format error.")
                #endif
                completion(nil, response.result.error)
                return
            }
            
            if let res = responseJSON as? [String:Any], (res["httpCode"] as? String) == "401" {
                self.reloadConfig(response: responseJSON as! DataResponse<Any>, completion: completion)
                return
            }
            
            #if DEBUG
                print("\(request.url?.absoluteString ?? ""): \(responseJSON)")
            #endif
            completion(responseJSON, nil)
        }
        
        return request
    }
    
    func reloadConfig (response: DataResponse<Any>, completion: @escaping (_ responseDict: Any?, _ error: Error?) -> Void) {
        #if DEBUG
        print("Credenziali APIC scadute.")
        #endif
        
        let error = NSError(domain: "", code: response.response?.statusCode ?? 0, userInfo: nil)
        
        if self.counter != 0 {
            self.counter -= 1
            
            self.getAPICConfig(completion: { ( _ ) in
                completion(nil, error)
            })
            
        } else {
            self.counter = IVPNetworkService.kCounter
            completion(nil, error)
        }
    }
    
    public func doLogin(username: String, password:String, completion: @escaping (_ response: String?, _ error: Error?) -> Void) throws {
        
        var request = URLRequest(url: IVPNetworkService.loginUrl)
        request.httpMethod = HTTPMethod.post.rawValue
        request.allHTTPHeaderFields = IVPNetworkService.loginHeaders
        
        //TOCO: Check se non c'è MFP rimuovere la WLAuthorizationManager.sharedInstance().login
        try request = URLEncoding.default.encode(request, with: ["username" : username.lowercased(), "password" : password])
//        WLAuthorizationManager.sharedInstance().login("", withCredentials: ["username": username.lowercased(), "password": password]) { (error) -> Void in
//            if let err = error {
//                #if DEBUG
//                    print("Error login MFP: \(String(describing: error))")
//                #endif
//
//                Crashlytics.sharedInstance().recordError(err)
//
//                completion(nil, error)
//                return
//            }
//
            IVPNetworkService.manager.request(request).responseData { response in
                guard response.result.isSuccess else {
                    #if DEBUG
                        print("Error login F5: \(String(describing: response.result.error))")
                    #endif
                    completion(nil, response.result.error)
                    return
                }
                
                let responseString = NSString(data: response.data!, encoding: 4)
                #if DEBUG
                    print("SERVICE: \(String(describing: responseString))")
                #endif
                completion(responseString as String?, nil)
            }
        //}
    }
    
    public func doLogout(completion: @escaping (_ response: String?) -> Void) throws {
        var request = URLRequest(url: IVPNetworkService.logoutUrl)
        request.httpMethod = HTTPMethod.get.rawValue
        request.allHTTPHeaderFields = IVPNetworkService.loginHeaders
        
        IVPNetworkService.manager.request(request).responseData { response in            
            guard response.result.isSuccess else {
                #if DEBUG
                    print("Error logout: \(String(describing: response.result.error))")
                #endif
                completion(nil)
                return
            }
            
            HTTPCookieStorage.shared.cookies?.forEach( {
                HTTPCookieStorage.shared.deleteCookie($0)
            })
            
            //USLoginManager.shared.clearCredentials()
            
            let responseString = NSString(data: response.data!, encoding: 4)
            #if DEBUG
                print("SERVICE: \(String(describing: responseString))")
            #endif
            completion(responseString as String?)
        }
    }
    
    // MARK: - static methods
    
    static let reachabilityManager = Alamofire.NetworkReachabilityManager(host: "www.apple.com")
    
    static func listenForReachability() {
//        let appDelegate = UIApplication.shared.delegate as? UnipolSaiAppDelegate
//
//        self.reachabilityManager?.listener = { status in
//            switch status {
//            case .notReachable:
//                appDelegate?.showAlertConnection()
//
//                break
//
//            case .reachable(_):
//                appDelegate?.hideAlertConnection()
//
//                break
//
//            case .unknown:
//                break
//            }
//        }
//
//        self.reachabilityManager?.startListening()
    }
    
    static func checkReachability() {
//        let appDelegate = UIApplication.shared.delegate as? UnipolSaiAppDelegate
//
//        let currentlyReachable = self.reachabilityManager?.isReachable ?? false
//
//        if (!currentlyReachable) {
//            appDelegate?.showAlertConnection()
//        }
    }
    
    public func getAPICConfig(completion: @escaping (_ response: String?) -> Void) {
        var request = URLRequest(url: IVPNetworkService.apicTokenUrl!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.allHTTPHeaderFields = IVPNetworkService.configHeaders
        
        callService(request: request) { (responseDict, error) in
            
            guard let token = responseDict as? [String : Any], let token_type = token["token_type"], let access_token = token["access_token"] else {
                #if DEBUG
                    print("SERVICE: no token_type or access_token")
                #endif
                completion(nil)
                return
            }
            
            var newRequest = URLRequest(url: IVPNetworkService.ibmClientStringsUrl)
            newRequest.httpMethod = HTTPMethod.get.rawValue
            let authString = "\(token_type) \(access_token)"
            newRequest.allHTTPHeaderFields = ["Authorization" : authString]
            
            self.authorization = ["Authorization" : authString]
            
            self.callService(request: newRequest, completion: { (ibmStringsDict, error) in
                guard let ibmDict = ibmStringsDict as? [String : Any], let clientId = ibmDict["x-ibm-client-id"], let clientSecret = ibmDict["x-ibm-client-secret"] else {
                    #if DEBUG
                        print("SERVICE: no token_type or access_token")
                    #endif
                    completion(nil)
                    return
                }
                
                self.headers = ["accept" : "application/json",
                                "x-ibm-client-id" : clientId as! String,
                                "x-ibm-client-secret" : clientSecret as! String,
                                "source" : "mobile"]
                
                completion("OK")
            })
        }
    }
}
