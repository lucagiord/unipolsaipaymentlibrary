//
//  Parser.swift
//  UnipolSaiPayments
//


import Foundation

class IVPDataParser: NSObject {
    
    public static let shared = IVPDataParser()
    
    public func parseMyParkings(json: [String : Any]) -> GetParkingsResponse? {
        
        guard let parkings = createParkingArray(data: json["parcheggi"]) else { return nil }
        
        let parkingResponse = GetParkingsResponse()
        parkingResponse.parkings = parkings
        return parkingResponse
        
    }
    
    public func parseMyParking(json: [String : Any]) -> GetParkingByIdResponse? {
        
        guard let parking = createSingleParking(json: json["parcheggio"]) else { return nil }
        
        let parkingById = GetParkingByIdResponse()
        parkingById.parking = parking
        return parkingById
        
    }
    
    public func parseMyParkingReservation(json: [String : Any]) -> SuccParkReservData? {
        
        let reservation = SuccParkReservData()
        
        guard let parking = createParkingShortInfo(data: json["parcheggio"]), let invoice = createParkingInvoice(data: json["fattura"]) else { return nil }
        
        reservation.parking = parking
        reservation.invoice = invoice
        
        return reservation
    }
    
    public func parseMyParkingInvoice(json: [String : Any]) -> CancelReservationParkingReq?  {
        
        guard let invoice = createParkingInvoice(data: json["fattura"]) else { return nil }
        
        let cancelReservation = CancelReservationParkingReq()
        cancelReservation.invoice = invoice
        return cancelReservation

    }
    
    public func parseParkingAccess(json: [String : Any]) -> ParkingCostsResponse? {
        
        guard let parking = createParkingStop(data: json["parcheggio"]), let invoice = createParkingInvoice(data: json["fattura"]) else { return nil }
        
        let parkingCostsResponse = ParkingCostsResponse()
        parkingCostsResponse.parking = parking
        parkingCostsResponse.invoice = invoice
        
        return parkingCostsResponse
        
    }
    
    public func parseParkingCostsEstimation(json: [String : Any]) -> ParkingAccessRequest? {
        
        guard let parking = createParkingEstimatedCost(data: json["parcheggio"]) else { return nil }
        
        let parkingAccessRequest = ParkingAccessRequest()
        parkingAccessRequest.parking = parking
        return parkingAccessRequest
    }
    
    public func parseUserExit(json: [String : Any]) -> ParkingCostsResponse? {
    
        guard let parking = createParkingStop(data: json["parcheggio"]), let invoice = createParkingInvoice(data: json["fattura"]) else { return nil }
        
        let parkingCostsResponse = ParkingCostsResponse()
        parkingCostsResponse.parking = parking
        parkingCostsResponse.invoice = invoice
        
        return parkingCostsResponse
        
    }
    
    public func parseUserPay(json: [String : Any]) -> ParkingPaymentResponse? {
        
        guard let parking = createParkingEnteringInfo(data: json["parcheggio"]), let invoice = createParkingInvoice(data: json["fattura"]) else { return nil }
        
        let parkingPaymentResponse = ParkingPaymentResponse()
        parkingPaymentResponse.parking = parking
        parkingPaymentResponse.invoice = invoice
        return parkingPaymentResponse
        
    }
    
    public func parseUserProfileData(json: [String : Any]) -> UserData? {
        
        guard let user = createUser(data: json["utente"]) else { return nil }
        
        let userData = UserData()
        userData.user = user
        return userData
        
    }
    
    public func parseParkingStatus(json: [String : Any]) -> GetStatusResponse? {
        
        guard let parkingStatus = createParkingStatus(data: json["stato"]) else { return nil }
        
        let stateResponse = GetStatusResponse()
        stateResponse.state = parkingStatus
        return stateResponse
        
    }
    
    public func parseUserPayments(json: [String : Any]) -> UserOrders? {
        
        guard let payments = createOrdersPaymentArray(data: json["pagamenti"]) else { return nil }
        
        let userOrders = UserOrders()
        userOrders.payments = payments
        return userOrders
        
    }
    
    public func parsePdfOrXslxGenerationRequest(json: [String : Any]) -> AccountBalanceResponse? {
        
        guard let document = json["documento"] as? String else { return nil }
        
        let accountBalanceResponse = AccountBalanceResponse()
        accountBalanceResponse.document = document
        
        if let rawValueDocumentType = json["tipologia"] as? Int {
            accountBalanceResponse.type = documentType(rawValue: rawValueDocumentType)
        }
        
        return accountBalanceResponse
        
    }
    
    public func parseEmptyResponse(json: [String : Any]) -> Bool {
        return json.count != 0
    }
    
    public func parsePaymentToken(json: [String : Any]) -> PaymentMethodResponse? {
        
        guard let tokenId = json["idToken"] as? String else { return nil }
        
        let paymentMethodToken = PaymentMethodResponse()
        paymentMethodToken.tokenId = tokenId
        paymentMethodToken.tokenAlias = json["aliasToken"] as? String
        
        if let rawValueResponseCodeEnum = json["codiceRisposta"] as? String {
            paymentMethodToken.responseCode = responseCodeEnum(rawValue: rawValueResponseCodeEnum)
        }
        
        paymentMethodToken.responseDescription = json["descrizioneRisposta"] as? String
        return paymentMethodToken
        
    }
    
    public func parseUserPaymentMethods(json: [String : Any]) -> ListPaymentMethodsResponse? {
        
        guard let paymentMethodsArray = createPaymentMethodArray(data: json["metodiPagamento"]) else { return nil }
        
        let listPaymentMethodsResponse = ListPaymentMethodsResponse()
        listPaymentMethodsResponse.listPaymentMethods = paymentMethodsArray
        return listPaymentMethodsResponse
        
    }
    
    public func parseStoppings(json: [String : Any]) -> Stoppings? {
        
        guard let stoppings = createStoppingsArray(data: json["soste"]) else { return nil }
        
        let stoppingsObj = Stoppings()
        stoppingsObj.stoppings = stoppings
        return stoppingsObj
        
    }
    
    public func parseUserDebts(json: [String : Any]) -> Debts? {
        
        guard let debtsArray = createUserDebtsArray(data: json["debiti"]) else { return nil }
        
        let debtsObj = Debts()
        debtsObj.debts = debtsArray
        return debtsObj
    }
    
    public func parseUserSingleDebt(json: [String : Any]) -> SingleDebt? {
        
        guard let userDebt = createSingleUserDebt(data: json["debito"]) else { return nil }
  
        let singleDebt = SingleDebt()
        singleDebt.debt = userDebt
        return singleDebt
        
    }
    
    public func parseReservations(json: [String : Any]) -> Reservations? {
        
        guard let reservationsArray = createReservationArray(data: json["prenotazioni"]) else { return nil }
        
        let reservations = Reservations()
        reservations.reservations = reservationsArray
        return reservations
        
    }
    
    private func createReservationArray(data: Any?) -> [Reservation]? {
        
        guard let reservationsArrayJson = data as? [[String : Any]] else { return nil }
        
        var reservationsArray: [Reservation] = []
        for reservationJson in reservationsArrayJson {
            if let reservation = createReservation(data: reservationJson) {
                reservationsArray.append(reservation)
            }
        }
        return reservationsArray
        
    }
    
    private func createReservation(data: Any?) -> Reservation? {
        
        guard let reservationJson = data as? [String : Any] else { return nil }
        
        let reservation = Reservation()
        reservation.reservationId = reservationJson["idPrenotazione"] as? String
        
        if var rawReservationParkingStatusEnum = reservationJson["statoParcheggio"] as? String {
            rawReservationParkingStatusEnum = rawReservationParkingStatusEnum.uppercased()
            reservation.parkingStatus = reservationParkingStatusEnum(rawValue: rawReservationParkingStatusEnum)
        }
        
        reservation.startingDate = reservationJson["dataInizio"] as? String
        reservation.endingDate = reservationJson["dataFine"] as? String
        reservation.parkingId = reservationJson["idParcheggio"] as? String
        reservation.licensePlate = reservationJson["targaVeicolo"] as? String
        return reservation
        
    }
    
    private func createSingleUserDebt(data: Any?) -> UserDebt? {
        
        guard let debt = createUserDebt(data: data) else { return nil }
        
        let userDebt = UserDebt()
        userDebt.debt = debt
        return userDebt
    }
    
    private func createUserDebtsArray(data: Any?) -> [Debt]? {
        
        guard let debtsArrayJson = data as? [[String : Any]] else { return nil }
        
        var debtsArray: [Debt] = []
        for debtJson in debtsArrayJson {
            if let debt = createUserDebt(data: debtJson) {
                debtsArray.append(debt)
            }
        }
        return debtsArray
    }
    
    private func createUserDebt(data: Any?) -> Debt? {
        
        guard let debtJson = data as? [String : Any] else { return nil }
        
        if let debtId = debtJson["idDebito"] as? String {
            
            let debt = Debt()
            debt.debtId = debtId
            
            if var rawValueDebtStatusEnum = debtJson["statoDebito"] as? String {
                rawValueDebtStatusEnum = rawValueDebtStatusEnum.uppercased()
                debt.debtStatus = debtStatusEnum(rawValue: rawValueDebtStatusEnum)
            }
            
            debt.amount = debtJson["importo"] as? Int
            debt.desc = debtJson["descrizione"] as? String
            
            if var rawValueDebtTypeEnum = debtJson["tipoDebito"] as? String {
                rawValueDebtTypeEnum = rawValueDebtTypeEnum.uppercased()
                debt.debtType = debtTypeEnum(rawValue: rawValueDebtTypeEnum)
            }
            
            debt.relatedEntity = debtJson["entitaCorrelate"] as? [String]
            debt.creationDate = debtJson["dataCreazione"] as? String
            debt.modificationDate = debtJson["dataModifica"] as? String
            
            return debt
            
        } else { return nil }
        
    }
    
    private func createStoppingsArray(data: Any?) -> [Stopping]? {
        
        guard let stoppingsArrayJson = data as? [[String : Any]] else { return nil }
        
        var stoppingsArray: [Stopping] = []
        for stoppingJson in stoppingsArrayJson {
            if let stopping = createStopping(data: stoppingJson) {
                stoppingsArray.append(stopping)
            }
        }
        return stoppingsArray
        
    }
    
    private func createParkingArray(data: Any?) -> [Parking]? {
        
        guard let parkingArrayJson = data as? [[String : Any]] else { return nil }
        
        var parkingArray: [Parking] = []
        for parkingJson in parkingArrayJson {
            if let parking = createSingleParking(json: parkingJson) {
                parkingArray.append(parking)
            }
        }
        return parkingArray
    }
    
    private func createPaymentMethodArray(data: Any?) -> [Plugin]? {
        
        guard let paymentMethodsArrayJson = data as? [[String : Any]] else { return nil }
        
        var paymentMethodsArray: [Plugin] = []
        for paymentMethodJson in paymentMethodsArrayJson {
            if let paymentMethod = createPaymentMethod(data: paymentMethodJson) {
                paymentMethodsArray.append(paymentMethod)
            }
        }
        return paymentMethodsArray
        
    }
    
    private func createPaymentMethod(data: Any?) -> Plugin? {
        
        guard let paymentMethodJson = data as? [String : Any] else { return nil }
        
        if let pluginName = paymentMethodJson["nomePlugin"] as? String {
            let paymentMethod = Plugin()
            paymentMethod.pluginName = pluginName
            paymentMethod.pluginData = createPluginDataArray(data: paymentMethodJson["datiPlugIn"])
            return paymentMethod
        } else { return nil }

    }
    
    private func createPluginDataArray(data: Any?) -> [PluginData]? {
        
        guard let pluginDataArrayJson = data as? [[String : Any]] else { return nil }
        
        var pluginDataArray: [PluginData] = []
        for pluginDataJson in pluginDataArrayJson {
            if let pluginData = createPluginData(data: pluginDataJson) {
                pluginDataArray.append(pluginData)
            }
        }
        return pluginDataArray
        
    }
    
    private func createPluginData(data: Any?) -> PluginData? {
        
        guard let pluginDataJson = data as? [String : Any] else { return nil }
        
        if let aliasToken = pluginDataJson["aliasToken"] as? String, let pan = pluginDataJson["pan"] as? String, let rawValuePaymentBrand = pluginDataJson["brand"] as? String, let expirationDate = pluginDataJson["dataScadenza"] as? String {
            let pluginData = PluginData()
            pluginData.isPreferred = pluginDataJson["preferito"] as? Bool ?? false
            pluginData.isPreauthorized = pluginDataJson["preautorizzato"] as? Bool ?? false
            pluginData.token = pluginDataJson["token"] as? String
            pluginData.aliasToken = aliasToken
            pluginData.pan = pan
            pluginData.brand = paymentBrand(rawValue: rawValuePaymentBrand)
            pluginData.expirationDate = expirationDate
            
            if var rawValueChannelType = pluginDataJson["canale"] as? String {
                rawValueChannelType = rawValueChannelType.uppercased()
                pluginData.channel = channelType(rawValue: rawValueChannelType)
            } else {
                pluginData.channel = .xpay
            }
            
            return pluginData
        } else { return nil }

    }
    
    private func createOrdersPaymentArray(data: Any?) -> [InvoiceResponse]? {
        
        guard let invoiceResponseArrayJson = data as? [[String : Any]] else { return nil }
        
        var invoiceResponseArray: [InvoiceResponse] = []
        for invoiceResponseJson in invoiceResponseArrayJson {
            if let invoiceResponse = createParkingInvoice(data: invoiceResponseJson) {
                invoiceResponseArray.append(invoiceResponse)
            }
        }
        return invoiceResponseArray
        
    }
    
    private func createParkingStatus(data: Any?) -> State? {

        guard let parkingStatusJson = data as? [String : Any] else { return nil }
        
        let parkingStatus = State()
        parkingStatus.parkings = createStoppingStatusArray(data: parkingStatusJson["parcheggi"])
        parkingStatus.amount = parkingStatusJson["importo"] as? Int
        parkingStatus.delFreeReservationRemain = parkingStatusJson["cancellazionePrenotazioniGratuiteRimaste"] as? Int
        parkingStatus.debts = parkingStatusJson["debiti"] as? Int
        return parkingStatus

    }
    
    private func createStoppingStatusArray(data: Any?) -> [StoppingState]? {
        
        guard let stoppingStatusArrayJson = data as? [[String : Any]] else { return nil }
        
        var stoppingStatusArray: [StoppingState] = []
        for stoppingStatusJson in stoppingStatusArrayJson {
            if let stoppingStatus = createStoppingStatus(data: stoppingStatusJson) {
                stoppingStatusArray.append(stoppingStatus)
            }
        }
        return stoppingStatusArray
        
    }
    
    private func createStoppingStatus(data: Any?) -> StoppingState? {
    
        guard let stoppingStatusJson = data as? [String : Any] else { return nil }
        
        if let stoppingId = stoppingStatusJson["idSosta"] as? String {
            
            let stoppingStatus = StoppingState()
            
            if var rawValueStoppingStatus = stoppingStatusJson["statoSosta"] as? String {
                rawValueStoppingStatus = rawValueStoppingStatus.uppercased()
                stoppingStatus.stoppingState = stoppingStateEnum(rawValue: rawValueStoppingStatus)
            }
            
            stoppingStatus.licensePlate = stoppingStatusJson["targa"] as? String
            stoppingStatus.parkingId = stoppingStatusJson["idParcheggio"] as? String
            stoppingStatus.amount = stoppingStatusJson["importo"] as? Int
            stoppingStatus.nextPaymentCost = stoppingStatusJson["costoProssimoPagamento"] as? Int
            stoppingStatus.secondsToNextPayment = stoppingStatusJson["secondiAlProssimoPagamento"] as? Int
            stoppingStatus.stoppingId = stoppingId
            stoppingStatus.reservationId = stoppingStatusJson["idPrenotazione"] as? String
            stoppingStatus.payments = stoppingStatusJson["pagamenti"] as? [String]
            stoppingStatus.exitTokens = createExitTokensArray(data: stoppingStatusJson["tokenUscita"])
            return stoppingStatus
            
        } else { return nil }
        
    }
    
    private func createExitTokensArray(data: Any?) -> [ExitToken]? {
        
        guard let exitTokenArrayJson = data as? [[String : Any]] else { return nil }
        
        var exitTokenArray: [ExitToken] = []
        for exitTokenJson in exitTokenArrayJson {
            if let exitToken = createExitToken(data: exitTokenJson) {
                exitTokenArray.append(exitToken)
            }
        }
        return exitTokenArray
        
    }
    
    private func createExitToken(data: Any?) -> ExitToken? {
        
        guard let exitTokenJson = data as? [String : Any] else { return nil }
        
        if let token = exitTokenJson["token"] as? String {
            
            let exitToken = ExitToken()
            exitToken.channel = exitTokenJson["canale"] as? String
            exitToken.token = token
            return exitToken
            
        } else { return nil }
        
        
    }
    
    private func createUser(data: Any?) -> User? {
        
        guard let userJson = data as? [String : Any] else { return nil }
        
        let user = User()
        user.name = userJson["nome"] as? String
        user.surname = userJson["cognome"] as? String
        user.phoneNumber = userJson["numeroTelefono"] as? String
        user.email = userJson["email"] as? String
        user.licensePlates = userJson["targheVeicolo"] as? [String]
        user.userCompanyId = userJson["idCompagniaUtente"] as? String
        user.idUserQBox = userJson["idUserQBox"] as? Int
        return user
        
    }
    
    
    private func createParkingEnteringInfo(data: Any?) -> ParkingEnteringInfo? {
        
        guard let parkingEnteringInfoJson = data as? [String : Any] else { return nil }
        
        if let parkingId = parkingEnteringInfoJson["idParcheggio"] as? String, let reservationId = parkingEnteringInfoJson["idPrenotazione"] as? String {
            
            let parkingEnteringInfo = ParkingEnteringInfo()
            parkingEnteringInfo.startDate = parkingEnteringInfoJson["dataInizio"] as? String
            parkingEnteringInfo.endDate = parkingEnteringInfoJson["dataFine"] as? String
            parkingEnteringInfo.parkingDuration = parkingEnteringInfoJson["durataParcheggio"] as? String
            parkingEnteringInfo.licensePlate = parkingEnteringInfoJson["targaVeicolo"] as? String
            parkingEnteringInfo.parkingId = parkingId
            parkingEnteringInfo.reservationId = reservationId
            
            if var rawValueEnteringState = parkingEnteringInfoJson["statoParcheggio"] as? String {
                rawValueEnteringState = rawValueEnteringState.uppercased()
                parkingEnteringInfo.parkingState = parkingEnteringState(rawValue: rawValueEnteringState)
            }
            
            parkingEnteringInfo.exitPin = parkingEnteringInfoJson["pinUscita"] as? String
            return parkingEnteringInfo
            
        } else { return nil }
        
    }
    
    private func createParkingEstimatedCost(data: Any?) -> ParkingEstimatedCost? {
        
        guard let parkingEstimatedCostJson = data as? [String : Any] else { return nil }
        
        let parkingEstimatedCost = ParkingEstimatedCost()
        parkingEstimatedCost.estimatedCost = createEstimatedCost(data: parkingEstimatedCostJson["costoStimato"])
        return parkingEstimatedCost
        
    }
    
    private func createEstimatedCost(data: Any?) -> EstimatedCost? {

        guard let estimatedCostJson = data as? [String : Any] else { return nil }

        if let estimationId = estimatedCostJson["idStima"] as? String, let parkingId = estimatedCostJson["idParcheggio"] as? String {
            
            let estimatedCost = EstimatedCost()
            estimatedCost.estimationId = estimationId
            estimatedCost.parkingId = parkingId
            estimatedCost.startDate = estimatedCostJson["dataInizio"] as? String
            estimatedCost.durationInHour = estimatedCostJson["durataInOre"] as? String
            estimatedCost.licensePlate = estimatedCostJson["targa"] as? String
            estimatedCost.totalAmount = estimatedCostJson["importoTotale"] as? String
            return estimatedCost
            
        } else { return nil }

    }
    
    private func createParkingStop(data: Any?) -> ParkingStop? {
        
        guard let parkingStopJson = data as? [String : Any] else { return nil }
        
        let parkingStop = ParkingStop()
        parkingStop.stopping = createStopping(data: parkingStopJson["sosta"])
        
        return parkingStop
        
    }
    
    private func createStopping(data: Any?) -> Stopping? {
        
        guard let stoppingJson = data as? [String : Any] else { return nil }
        
        if let stoppingId = stoppingJson["idSosta"] as? String {
          
            let stopping = Stopping()
            stopping.stoppingId = stoppingId
            stopping.parkingId = stoppingJson["idParcheggio"] as? String
            stopping.parkingName = stoppingJson["nomeParcheggio"] as? String
            stopping.exitPin = stoppingJson["pinUscita"] as? String
            stopping.stoppingStartDate = stoppingJson["dataInizioSosta"] as? String
            stopping.stoppingEndDate = stoppingJson["dataFineSosta"] as? String
            stopping.licensePlate = stoppingJson["targaVeicolo"] as? String

            if var rawValueStoppingStateEnum = stoppingJson["statoParcheggio"] as? String {
                rawValueStoppingStateEnum = rawValueStoppingStateEnum.uppercased()
                stopping.parkingStatus = stoppingStateEnum(rawValue: rawValueStoppingStateEnum)
            }
            
            stopping.amount = stoppingJson["importo"] as? String
            stopping.payments = createStoppingPaymentArray(data: stoppingJson["pagamenti"])
            return stopping
            
        } else { return nil }

    }
    
    private func createStoppingPaymentArray(data: Any?) -> [Payment]? {
        
        guard let paymentArrayJson = data as? [[String : Any]] else { return nil }
        
        var paymentArray: [Payment]? = []
        for paymentJson in paymentArrayJson {
            if let payment = createStoppingPayment(data: paymentJson) {
                paymentArray?.append(payment)
            }
        }
        return paymentArray
    }
    
    private func createStoppingPayment(data: Any?) -> Payment? {
        
        guard let paymentJson = data as? [String : Any] else { return nil }
        
        if let paymentId = paymentJson["idPagamento"] as? String {
            
            let payment = Payment()
            payment.paymentId = paymentId
            
            if var rawValueTransactionResultEnum = paymentJson["esitoTransazione"] as? String {
                rawValueTransactionResultEnum = rawValueTransactionResultEnum.uppercased()
                payment.transationResult = transactionResultEnum(rawValue: rawValueTransactionResultEnum)
            }
            
            payment.payedAmount = paymentJson["importoPagato"] as? String
            payment.remainedAmount = paymentJson["importoResiduo"] as? String
            
            if var rawValuePaymentMethodEnum = paymentJson["metodiPagamento"] as? String {
                rawValuePaymentMethodEnum = rawValuePaymentMethodEnum.uppercased()
                payment.paymentMethods = paymentMethodEnum(rawValue: rawValuePaymentMethodEnum)
            }
            
            return payment

        } else { return nil }
        
    }
    
    private func createParkingShortInfo(data: Any?) -> ParkingShort? {
        
        guard let parkingJson = data as? [String : Any] else { return nil }
        
        if let reservationId = parkingJson["idPrenotazione"] as? String {
           
            let parking = ParkingShort()
            parking.reservationId = reservationId
            parking.reservationStatus = parkingJson["statoPrenotazione"] as? String
            parking.reservationDate = parkingJson["dataPrenotazione"] as? String
            parking.reservationEndDate = parkingJson["dataFinePrenotazione"] as? String
            parking.parkingId = parkingJson["idParcheggio"] as? String
            parking.licensePlate = parkingJson["targa"] as? String
            parking.idStop = parkingJson["idSosta"] as? String
            return parking
            
        } else { return nil }
    }
    
    private func createParkingInvoice(data: Any?) -> InvoiceResponse? {
        
        guard let invoiceJson = data as? [String : Any] else { return nil }
        
        if let reservationId = invoiceJson["idOrdine"] as? String {
            
            let invoice = InvoiceResponse()
            invoice.reservationId = reservationId
            invoice.reservationType = invoiceJson["tipoOrdine"] as? String
            invoice.partnerId = invoiceJson["idPartner"] as? String
            invoice.userId = invoiceJson["idUtente"] as? String
            invoice.idBillableAccount = invoiceJson["idBillableAccount"] as? String
            invoice.creationDate = invoiceJson["dataCreazione"] as? String
            invoice.reservationData = createReservationData(data: invoiceJson["datiOrdine"])
            invoice.requests = createParkingRequestsArray(data: invoiceJson["richieste"])
            invoice.detailData = createInvoiceDetailData(data: invoiceJson["datiDettaglio"])
            return invoice
            
        } else { return nil }
        
    }

    
    private func createSingleParking(json: Any?) -> Parking? {
        
        guard let parkingJson = json as? [String : Any] else { return nil }
        
        var enumParkingState: parkingState? = nil
        
        if var rawValueParkingState = parkingJson["stato"] as? String {
            rawValueParkingState = rawValueParkingState.uppercased()
            enumParkingState = parkingState(rawValue: rawValueParkingState)
        }
        
        if let enumParkingState = enumParkingState {
            if enumParkingState == .prossima_apertura {
                return nil
            }
        }
        
        if let parkingId = parkingJson["idParcheggio"] as? String {
            
            let parking = Parking()
            parking.parkingId = parkingId
            parking.businessName = parkingJson["businessNome"] as? String
            parking.parkingName = parkingJson["nomeParcheggio"] as? String
            parking.parkingAddress = createParkingAddressObj(data: parkingJson["indirizzoParcheggio"])
            parking.latitude = parkingJson["latitudine"] as? Double
            parking.longitude = parkingJson["longitudine"] as? Double
            parking.timeZone = parkingJson["timeZone"] as? String
            parking.parkingPhone = parkingJson["telefonoParcheggio"] as? String
            parking.maxHeightAllowed = parkingJson["altezzaMassimaConsentitaInCm"] as? Int
            
            if var rawValueAccessControl = parkingJson["tipoControlloAccesso"] as? String {
                rawValueAccessControl = rawValueAccessControl.uppercased()
                parking.accessControlType = accessControlType(rawValue: rawValueAccessControl)
            }
        
            parking.state = enumParkingState
            parking.qBoxConfiguration = createQBoxConfArray(data: parkingJson["configurazioneQBox"])
            parking.secondsToClose = parkingJson["secondiAllaChiusura"] as? Int
            parking.nextOpeningInfo = createNextOpeningInfo(data: parkingJson["informazioniProssimaApertura"])
            parking.automationProviderConfiguration = createAutomationProviderConfiguration(data: parkingJson["configurazioneProviderAutomazione"])
            parking.additionalServices = createAdditionalServices(data: parkingJson["serviziAggiuntivi"])
            parking.prices = createParkingPrices(data: parkingJson["prezzi"])
            
            return parking
            
        } else { return nil }
    }
    
    private func createParkingAddressObj(data: Any?) -> ParkingAddress? {
        
        guard let addressJson = data as? [String : Any] else { return nil }
        
        let parkingAddress = ParkingAddress()
        parkingAddress.address = addressJson["indirizzo"] as? String
        parkingAddress.city = addressJson["citta"] as? String
        parkingAddress.cap = addressJson["cap"] as? String
        parkingAddress.province = addressJson["provincia"] as? String
        parkingAddress.nation = addressJson["nazione"] as? String
        return parkingAddress
        
    }
    
    private func createQBoxConfArray(data: Any?) -> [QBoxConfigurationData]? {
        
        guard let qBoxConfJson = data as? [[String : Any]] else { return nil }
        
        var configurations: [QBoxConfigurationData] = []
        
        for qBoxConf in qBoxConfJson {
            let conf = QBoxConfigurationData()
            conf.qBoxId = qBoxConf["qBoxId"] as? Int
            conf.info = qBoxConf["info"] as? String
            configurations.append(conf)
        }
        return configurations
        
    }

    private func createNextOpeningInfo(data: Any?) -> NextOpeningInfo? {
        
        guard let nextOpeningInfoJson = data as? [String : Any] else { return nil }
        
        let nextOpeningInfo = NextOpeningInfo()
        nextOpeningInfo.nextOpening = nextOpeningInfoJson["prossimaApertura"] as? String
        nextOpeningInfo.nextOpeningDetail = nextOpeningInfoJson["dettagliProssimaApertura"] as? String
        return nextOpeningInfo
        
    }
    
    private func createAutomationProviderConfiguration(data: Any?) -> AutomationProviderConfiguration? {
        
        guard let json = data as? [String : Any] else { return nil }
        
        let provider = AutomationProviderConfiguration()
        provider.hasPedestrianEntrance = json["hasIngressoPedonale"] as? Bool
        provider.automationProviderCode = (json["codiceProviderAutomazione"] as? Int) ?? 0
        provider.integrationProtocolCode = (json["codiceProtocolloIntegrazione"] as? Int) ?? 0
        return provider
        
    }
    
    private func createAdditionalServices(data: Any?) -> [parkingAdditionalServices]? {
        
        guard let services = data as? [String] else { return nil }
        
        var parkingServices: [parkingAdditionalServices] = []
        for service in services {
            guard let enumValue = parkingAdditionalServices(rawValue: service) else { return nil }
            parkingServices.append(enumValue)
        }
        return parkingServices
        
    }
    
    private func createParkingPrices(data: Any?) -> Prices? {
        
        guard let pricesJson = data as? [String : Any] else { return nil }
        
        let prices = Prices()
        
        if var rawValueCarCategory = pricesJson["segmento"] as? String {
            rawValueCarCategory = rawValueCarCategory.uppercased()
            prices.segment = carCategory(rawValue: rawValueCarCategory)
        }
        
        prices.openingTimes = createParkingOpeningTimes(data: pricesJson["orariApertura"])
        prices.maxDailyPrice = pricesJson["prezzoMassimoGiornaliero"] as? Int
        
        return prices
        
    }
    
    private func createParkingOpeningTimes(data: Any?) -> [OpeningTime]? {
        
        guard let openingTimesJson = data as? [[String : Any]] else { return nil }
        
        var openingTimes: [OpeningTime] = []
        
        for json in openingTimesJson {
            let openingTime = OpeningTime()
            
            if var rawValueWeekDays = json["giorno"] as? String {
                rawValueWeekDays = rawValueWeekDays.uppercased()
                openingTime.day = openingDays(rawValue: rawValueWeekDays)
            }
            
            openingTime.timeIntervals = createParkingOpeningTimeIntervals(data: json["intervalliOrari"])
            openingTime.isOpen24Hours = json["aperto24ore"] as? Bool
            openingTime.isClose = json["chiuso"] as? Bool
            
            openingTimes.append(openingTime)
        }
        
        return openingTimes
    }
    
    private func createParkingOpeningTimeIntervals(data: Any?) -> [TimeInterval]? {
        
        guard let timeIntervalsJson = data as? [[String : Any]] else { return nil }
        
        var timeIntervals: [TimeInterval] = []
        
        for json in timeIntervalsJson {
            let timeInterval = TimeInterval()
            timeInterval.startingTime = json["orarioInizio"] as? String
            timeInterval.endingTime = json["orarioTermine"] as? String
            timeInterval.parkingPrices = createParkingPricesDetail(data: json["prezziParcheggio"])
            timeIntervals.append(timeInterval)
        }
        
        return timeIntervals
    }
    
    private func createParkingPricesDetail(data: Any?) -> [ParkingPrice]? {
        
        guard let parkingPricesJson = data as? [[String : Any]] else { return nil }
        
        var parkingPrices: [ParkingPrice] = []
        
        for json in parkingPricesJson {
            let parkingPrice = ParkingPrice()
            parkingPrice.time = json["ora"] as? Int
            parkingPrice.price = json["prezzo"] as? Int
            parkingPrices.append(parkingPrice)
        }
        
        return parkingPrices
    }
    
    private func createReservationData(data: Any?) -> ReservationData? {
        
        guard let reservationDataJson = data as? [String : Any] else { return nil }
        
        let reservationData = ReservationData()
        reservationData.reservationDataInner = createReservationDataInner(data: reservationDataJson["dati"])
        return reservationData
        
    }
    
    private func createReservationDataInner(data: Any?) -> ReservationDataInner? {
        
        guard let reservationDataInnerJson = data as? [String : Any] else { return nil }
        
        let reservationDataInner = ReservationDataInner()
        reservationDataInner.licensePlate = reservationDataInnerJson["targa"] as? String
        return reservationDataInner
        
    }
    
    private func createParkingRequestsArray(data: Any?) -> [ParkingRequest]? {
        
        guard let parkingRequestsJson = data as? [[String : Any]] else { return nil }
        
        var parkingRequests: [ParkingRequest] = []
        for parkingRequestJson in parkingRequestsJson {
            if let parkingRequest = createParkingRequest(data: parkingRequestJson) {
                parkingRequests.append(parkingRequest)
            }
        }
        return parkingRequests
    }
    
    private func createParkingRequest(data: Any?) -> ParkingRequest? {
        
        guard let parkingRequestJson = data as? [String : Any] else { return nil }
        
        if let requestId = parkingRequestJson["idRichiesta"] as? String {
            
            let parkingRequest = ParkingRequest()
            parkingRequest.requestId = requestId
            parkingRequest.requestType = parkingRequestJson["tipoRichiesta"] as? String
            parkingRequest.creationDate = parkingRequestJson["dataCreazione"] as? String
            parkingRequest.amount = parkingRequestJson["importo"] as? Int
            parkingRequest.idPaymentMethod = parkingRequestJson["idMetodoPagamento"] as? String
            parkingRequest.receiptInformations = createParkingReceiptInfoArray(data: parkingRequestJson["informazioniRicevuta"])
            return parkingRequest
            
        } else { return nil }

    }
    
    private func createParkingReceiptInfoArray(data: Any?) -> [ReceiptInfo]? {
        
        guard let receiptInformationsJson = data as? [[String : Any]] else { return nil }
        
        var receiptInformations: [ReceiptInfo] = []
        for infoJson in receiptInformationsJson {
            if let info = createParkingReceiptInfo(data: infoJson) {
                receiptInformations.append(info)
            }
        }
        return receiptInformations
        
    }
    
    private func createParkingReceiptInfo(data: Any?) -> ReceiptInfo? {
        
        guard let infoJson = data as? [String : Any] else { return nil }
        
        let info = ReceiptInfo()
        info.date = infoJson["data"] as? String
        info.descr = infoJson["descrizione"] as? String
        info.amount = infoJson["importo"] as? Int
        
        if var rawValueRequestStatus = infoJson["statoRichiesta"] as? String {
            rawValueRequestStatus = rawValueRequestStatus.uppercased()
            info.requestStatus = requestStatus(rawValue: rawValueRequestStatus)
        }
        
        info.serviceData = createServiceDataInvoice(data: infoJson["datiServizio"])
        info.payments = createInvoicePaymentsArray(data: infoJson["pagamenti"])
        return info
        
    }
    
    private func createServiceDataInvoice(data: Any?) -> ServiceDataInvoice? {
        
        guard let serviceDataInvoiceJson = data as? [String : Any] else { return nil }
        
        let serviceDataInvoice = ServiceDataInvoice()
        serviceDataInvoice.inStructureParkingData = createInStructureParkingData(data: serviceDataInvoiceJson["datiParcheggioInStruttura"])
        return serviceDataInvoice
        
    }
    
    private func createInStructureParkingData(data: Any?) -> InStructureParkingData? {
        
        guard let inStructureParkingDataJson = data as? [String : Any] else { return nil }
    
        if let parkingStart = inStructureParkingDataJson["inizioSosta"] as? String, let parkingEnd = inStructureParkingDataJson["fineSosta"] as? String {
            
            let inStructureParkingData = InStructureParkingData()
            inStructureParkingData.parkingStart = parkingStart
            inStructureParkingData.parkingEnd = parkingEnd
            return inStructureParkingData
            
        } else { return nil }
        
    }
    
    private func createInvoicePaymentsArray(data: Any?) -> [Payments]? {
        
        guard let paymentsJson = data as? [[String : Any]] else { return nil }
        
        var payments: [Payments] = []
        for paymentJson in paymentsJson {
            if let payment = createPayment(data: paymentJson) {
                payments.append(payment)
            }
        }
        return payments
        
    }
    
    private func createPayment(data: Any?) -> Payments? {
        
        guard let paymentJson = data as? [String : Any] else { return nil }
        
        let payment = Payments()
        payment.paymentIdPay = createPaymentIdPay(data: paymentJson["pagamentoIdPay"])
        payment.paymentPayTipper = createPaymentPayTipper(data: paymentJson["pagamentoPayTipper"])
        return payment
        
    }
    
    private func createPaymentIdPay(data: Any?) -> PaymentIdPay? {
        
        guard let paymentIdPayJson = data as? [String : Any] else { return nil }
        
        if let keyApp = paymentIdPayJson["keyApp"] as? String {
            let paymentIdPay = PaymentIdPay()
            paymentIdPay.keyApp = keyApp
            paymentIdPay.amount = paymentIdPayJson["importo"] as? Int
            paymentIdPay.aliasToken = paymentIdPayJson["aliasToken"] as? String
            
            return paymentIdPay
        } else { return nil }
        
    }
    
    private func createPaymentPayTipper(data: Any?) -> PaymentPayTipper? {
        
        guard let paymentPayTipperJson = data as? [String : Any] else { return nil }
        
        if let paymentId = paymentPayTipperJson["idPagamento"] as? Int {
            
            let paymentPayTipper = PaymentPayTipper()
            
            if var rawValuePaymentStatus = paymentPayTipperJson["statoPagamento"] as? String {
                rawValuePaymentStatus = rawValuePaymentStatus.uppercased()
                paymentPayTipper.paymentStatus = payTipperStatus(rawValue: rawValuePaymentStatus)
            }
            
            paymentPayTipper.paymentId = paymentId
            paymentPayTipper.paymentDate = paymentPayTipperJson["dataPagamento"] as? String
            paymentPayTipper.controlKey = paymentPayTipperJson["chiaveControllo"] as? String
            
            return paymentPayTipper
            
        } else { return nil }
        
    }
    
    private func createInvoiceDetailData(data: Any?) -> ReservationParkingDetails? {
        
        guard let reservationParkingDetailsJson = data as? [String : Any] else { return nil }
        
        let reservationParkingDetails = ReservationParkingDetails()
        reservationParkingDetails.totalAmount = reservationParkingDetailsJson["importoTotale"] as? Int
        reservationParkingDetails.startDate = reservationParkingDetailsJson["dataInizio"] as? String
        reservationParkingDetails.endDate = reservationParkingDetailsJson["dataFine"] as? String
        reservationParkingDetails.summary = createInvoiceSummaryArray(data: reservationParkingDetailsJson["riepilogo"])
        return reservationParkingDetails
        
    }
    
    private func createInvoiceSummaryArray(data: Any?) -> [ReservationSummary]? {
        
        guard let reservationSummaryArrayJson = data as? [[String : Any]] else { return nil }
        
        var reservationSummaryArray: [ReservationSummary] = []
        for reservationSummaryJson in reservationSummaryArrayJson {
            if let summary = createReservationSummary(data: reservationSummaryJson) {
                reservationSummaryArray.append(summary)
            }
        }
        return reservationSummaryArray
        
    }
    
    private func createReservationSummary(data: Any?) -> ReservationSummary? {
        
        guard let reservationSummaryJson = data as? [String : Any] else { return nil }
        
        let summary = ReservationSummary()
        summary.amount = reservationSummaryJson["importo"] as? Int
        summary.date = reservationSummaryJson["data"] as? String
        summary.descr = reservationSummaryJson["descrizione"] as? String
        return summary
        
    }
    
}
