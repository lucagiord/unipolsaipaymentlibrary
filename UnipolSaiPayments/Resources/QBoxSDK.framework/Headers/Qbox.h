//
//  Qbox.h
//  r2p
//
//  Created by alvaro mendoza on 30/11/17.
//  Copyright © 2017 r2p. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    GateTypeENTRY,
    GateTypeEXIT,
    GateTypeMANUAL
}GateType;

@interface Qbox : NSObject

@property (nonatomic,strong) NSData *instanceId;
@property (nonatomic,strong) NSData *nameSpace;
@property (nonatomic) Byte *lane;
@property (nonatomic) GateType type;

-(void)setQboxFromDictionary:(NSDictionary*)dictionary;

@end
