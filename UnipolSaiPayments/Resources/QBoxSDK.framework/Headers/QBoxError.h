//
//  QBoxError.h
//  QBoxSDK
//
//  Created by Alvaro Mendoza on 11/1/18.
//

#import <Foundation/Foundation.h>

typedef enum {
    MISSING_COARSELOCATION_PERMISSION=1,
    BLUETOOTH_DISABLED=2,
    COMMUNICATION_TIMEOUT=3,
    WRONG_TOKEN=4
}errorCode;

NS_ASSUME_NONNULL_BEGIN

@interface QBoxError : NSObject

@property (nonatomic) int errorCode;
@property (nonatomic,strong) NSError *error;

@end

NS_ASSUME_NONNULL_END
