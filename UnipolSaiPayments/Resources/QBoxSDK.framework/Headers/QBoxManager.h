//
//  QBoxManager.h
//  QBoxSDK
//
//  Created by Alvaro Mendoza on 10/23/18.
//  Copyright © 2018 Alvaro Mendoza. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "QBoxError.h"
#import "Qbox.h"




NS_ASSUME_NONNULL_BEGIN
@protocol QBoxManagerDelegate<NSObject>
- (void)onQBoxFound:(Qbox*)qbox andDistance:(double)distance;
- (void)onEntered:(Qbox*)qbox andToken:(NSString*)token;
- (void)onExited:(Qbox*)qbox;
- (void)onError:(QBoxError *)error;
- (void)onSignaled:(Qbox*)qbox;
@end

@interface QBoxManager : NSObject <CBCentralManagerDelegate,CBPeripheralDelegate>

@property (nonatomic, strong) id <QBoxManagerDelegate> delegate;


+ (QBoxManager*)sharedInstance;
- (BOOL)startEntryQBoxScan;
- (BOOL)startExitQBoxScan;
- (void)sendEnterRequestToQbox:(Qbox*)qbox andUserId:(int)userId;
- (void)sendExitRequestToQbox:(Qbox*)qbox andToken:(NSString*)token;
- (void)sendSignalRequest:(Qbox*)qbox andUserId:(int)userId;
- (BOOL)stopQBoxScan;
@end

NS_ASSUME_NONNULL_END
