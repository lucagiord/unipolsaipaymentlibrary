//
//  QBoxSDK.h
//  QBoxSDK iOS
//
//  Created by Alvaro Mendoza on 10/29/18.
//

#import <UIKit/UIKit.h>

//! Project version number for QBoxSDK.
FOUNDATION_EXPORT double QBoxSDKVersionNumber;

//! Project version string for QBoxSDK.
FOUNDATION_EXPORT const unsigned char QBoxSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <QBoxSDK/PublicHeader.h>

#import <QBoxSDK/QBoxManager.h>
#import <QBoxSDK/Qbox.h>


